const countryList = require("./src/countyList/countries.json");


function loadCountryList() {
  return countryList;
}

function loadCountry(isoCode){
  return countryList.find((country)=>country.isoCode.toLowerCase() ===isoCode)
}


module.exports = {
  loadCountryList,
  loadCountry
};
