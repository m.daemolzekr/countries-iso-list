declare module 'countries-iso-list' {
    export function loadCountryList(): any[];
    export function loadCountry(isoCode:string): any;
  }