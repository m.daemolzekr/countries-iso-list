const { getCountryData } = require('../../index');  

test('getCountryData returns the correct data', async () => {
  const germanyData = await getCountryData('de');
  expect(germanyData.name).toBe('Germany');
  expect(germanyData.phonePrefix).toBe('+49');
  expect(germanyData.isoCode).toBe('DE');
});